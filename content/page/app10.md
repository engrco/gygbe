---
title: Virtual Toastmaster GYG
subtitle: Public speaking is all about PRACTICING
comments: false
---

***WARNING: The first RULE of every GYG is to first look around and then critically review/revise or even completely throw out the following background for a GYG ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

This GYG has to be about PRACTICING public speaking ... which, as we know from Toastmasters, is really about facilitating that PRACTICE for others and improving one's speaking by helping others improve their speaking ... and since GYG is strongly adverse to any form of time wasting and commuting to yet another meeting or conference, this GYG is ultimately about setting up, running, monitoring and improving VIRTUAL events that facilitate that practice.

It could be distraction of AI/ML critiquor of speaking / presentation abilities ... which would get us used to thinking about [event stream use cases](https://kafka.apache.org/documentation/#uses)... helps for making the most of virtual confs, ie elevator speechs, getting point across in 10 minutes, telescoping out to git repositories of Jupyter notebooks ... but that's probably mostly just a distraction.

It has to be about PRACTICING ... that means attending virtual events, meeting people, and practicing the craft of VIRTUAL presentation and speaking.

## Attend Virtual Events

Of course, you can and really should READ about [examples of what's happening at virtual events](https://hopin.com/blog/global-virtual-events-examples) ... as a matter of preparation for thinking about how you are going to PRACTICE ... but there's no substitute for attending and sort of throwing yourself into the fray -- because, as long as you are generally respectful and kind to others, no one is going to remember ... you need the practice ... to practice a million times ... more than you can imagine ... attend to get more practice ... especially in the realm of interactions.

### Be able to critique a presentation; so that you don't make the same mistakes

For example, you might already know from watching recording presentations on YouTube that 99.999% of presentations which happen at conferences are barely watchable and must be FFWDd through, ie if they don't have a time-stamped outline with slides, Git repositories and the appropriate hyperlinks in the Description those videos can and should be avoided altogether.

## Organize Your Own Virtual Event

BEFORE you start ... understand that an *event* is suppposed to be ***EVENTFUL*** in a positive way for every single attendee. *It's not YOUR puny-ass, self-centered event -- you serve them; it is THEIR event.*

This means that recorded video [which might be a nice addition] misses the point and essentially DISTRACTS event organizers as well as attendees from what the EVENT is about ... if they are there for info presentation, why not JUST record a decent presentation with bullet-pointed outline, slides and content repository ... an event is an EVENT.

Virtual CONFERENCES are also not just large virtual Zoom meetings, Conferences are not about meetings [where people know each other] but rather are about ENGAGEMENT between people who might share interests but do not know each other ... it's about the [serindipitous] interactions that happen OUTSIDE of the presentations, NOT just engagement with presenters [after/before their talk], but engagement with attendees.  

A successful virtual event SHOULD be like the most successful party ever ... in which lots of people who visit the event to make connections get to see old friends and also make brand new connections [which might lead to things, might not and might lead to new connections that lead to something] ... and virtual events should be SAFE ... NOBODY should get pushed into an uncomfortable corner pigeonholed by a jerk [or criminal] or gets pushed into doing something they didn't want to do.

If you want to ORGANIZE a virtual event ... you have to think primarily about safe ENGAGEMENT ... and giving others the floor to speak or coming up with gamification strategies to increasing serindipitous interactions ... rather than just speaking yourself ... you will be the center of attention for organizing and hosting the great party.

### Building your virtual event strategy

1. First things first: EXACTLY what do you want to accomplish with your event?

Before any preparation, brainstorming, or promotional activity can happen, you need to have a clear understanding of WHY you want to host an event.  WHY should anyone bother?

Once you know those broader goals, identify the Key Process Indicators (KPIs) or targets that will help you measure your success against those goals. Metrics have nothing to do with you getting some sort of accolades -- **KPIs are the metrics you use in order find out where you suck, so that you can get better next time**

The examples that people typically think of will include:

Total registrations
Total attendance
Event net promoter score—or NPS
Ticket sales
Sponsorship sales
Total revenue
Total profit

You can also set KPIs for interaction and audience engagement—such as average time in event per attendee or expo booth visits.

Setting engagement goals like this can help your team focus on creating a truly interactive experience for attendees.

It bears repeating ... metrics aren't for accolades; metrics are not about being like; they are about disappointments. *KPIs must be designed to find out where you suck, so that you can get better next time*  You want to find those things that you work on. You will also find those areas which have been been allocated effort/resources that can be reallocated to the areas that need work. That means ... **metrics are about quantitative re-adjustment ... when your metrics have told you no adjustments are needed, your metrics have failed.**

2. Determine your budget.
A budget provides the parameters for your event. It helps you understand what you’re able to do and not do. Knowing your budget allows you to host the best possible event with the resources you have. Consider your revenue and profit goals, if possible, ie at lease TRY to play offense or get the cost to zero ... do not assume that you are entitled to one penny.  

If your event cannot be revenue positive, it is going to HAVE TO generate some undeniably HUGE benefits in other realms. 

**Start with the most minimal of all MINIMALIST approaches ... or don't hold the event.** Identify the LEANEST Minimum Viable Product budget you need to invest in your event and deliver on your revenue targets, while staying cashflow positive and hitting your profit targets. You will KNOW, with absolute quantitative certainty, that your event needs something larger than the MVP budget.

3. Identify your audience. 

Events start with UNDERSTANDING your target audience.  You MUST understand your audience ... first ... BEFORE you begin to build an event for them.

Once you REALLY understand your audience, you will be able to program content that is something that they NEED to do.  If your event is something that is not clearly better than the competing uses of your event audience, you shouldn't be in the event business.

You have to think about WHO is going to CARE enough about your event to attend ... beyond just that, why are they going to CARE even more AFTER the event and tell everyone they know to come to next year's event? Do not ever hold an event that is not going to be a better event next year ... if your event doesn't have this kind of growth target, you are really doing something like a bad funeral, ie a guilt trip that everyone dreads -- if that's the case, you shouldn't really be in the event business.

4. Build your DIVERSE event team. 

Once you know your goals and general event scope, you can assembly your event team ... but it's likely that, if you have been listening well and communicating, members of your event team are going to assemble themselves ... never take prisoners, never use slaves, never beg people to be involved in your pathetic event ... ATTRACT a team; have specific items, when they ask what they can do.

To be sure, the specific resources you need will depend on your event program. However, a core team will be instrumental to parsing out the details of your event.

Your event team may consist of employees, agencies, and/or freelancers. Begin with the work that needs to be done, and bring in outside people accordingly, where you or your team lacks skills ... make sure that you have things covered:

* Communication and coordination within your team and with partners, sponsors, speakers, and attendees
* Marketing for social media, press releases, and partner marketing
* Copywriting for landing pages, event descriptions, and email
* Event production and design
* Data analysis for KPI tracking
* Technical support for attendee and speaker questions or technical issues

Diversity of your team is an absolutely extra-NECESSARY factor ... maybe it seems that way now more than ever, but diversity always WAS necessary, especially if you didn't get it before.  

A team comprised of a range of backgrounds and perspectives can often surface new ideas and help guard against blind spots that can affect the success of your event. Thus, diversity is NECESSARY for making things better; avoid bringing in *like-minded* people; it is human nature that you will already have way more enough of people who think like you do around you. **True diversity is about aggressively COMPLEMENTING, not complimenting, your team. Never just REPLICATED your team or pick people just because your team is comfortable with them.**

5. Choose an event theme or topic.

Parties and events are better because of themes or topics ... they have to be CURRENT ... consider the Rolling Stones #NoFilter theme as perfect choice of a theme following a year of heavy, abusive filtering, censorship and cancel culture. By early 2021, everyone was SICK and TIRED of filters ... so #NoFilter was a perfect theme for a band of old white guys who still kick ass and take names. 

Avoid hosting an event that is nearly identical to or redudant of events that have already occurred ... you cannot piggyback on the Stone's #NoFilter Tour without looking like the painfully unaware group of Microsoft executives launching Windows95 to the always cool "Start Me Up" Stones song, but you can RUIN that song permanently if you wanna pay for it.

Brainstorm at least 200 ideas for an event theme ... list all ideas ... good ideas might not appear all that great until after they are reviewed ... the first 50 or 100 or so of your brainstormed will tend be the pet ideas you've had, but seriously are not going to EVER go anywhere ... the next 50 or so will be bad ideas, but they will get you to think. Getting the last 50 ideas of your brainstorming session will be almost painful and tedious ... but you need to get to the point where you SICK of the process to be able to review the ideas and find the ONE with legitimate POP.

Avoid going with a POPULAR, safe theme ... go with a DISRUPTIVE, UNFORGETABLE, truly memorable dangerous theme.
 
6. Define your event program and type.
There are a multitude of options to think through when defining your virtual event’s program or structure. 

It can be overwhelming, so here is a list of questions you can ask yourself when creating your event program:

Who is invited? For external events (i.e., for people outside your company), do you want to attract a broad audience or a specific customer segment? Same goes for internal events: Are you inviting your full company or a single team? Remember, your content flows from who your audience is.
Where is it? You can host an entirely online event. Or you can host a hybrid event, which combines physical and online components to get the best of both worlds: in-person engagement and the reach of online.
What is the event format? Depending on your event goals and specific use case, there are dozens of options—conferences, summits, trade shows, recruiting fairs, workshops, social gatherings, etc.
What are the session types? Again, there are many options here—keynotes, panel discussions, fireside chats, workshops, product demos, tours, etc. Engaging online events are those that vary the session types and content within a single event.
How will you drive participation? Audience engagement is central to a successful virtual event. You can enable participation from your online audience via video Q&A, one-on-one video networking, small-group meet-and-greets, chat, polls, etc.
Will you include social activities? Beyond sharing ideas and information, a major benefit of events generally is their ability to foster connection and generate goodwill. You can realize that benefit by programming social activities or group entertainment, such as virtual event games, after-parties, DJs, magic shows, virtual scavenger hunts, etc.
Answering these questions will help you start to hone your program and your content.

7. Pick your date and time, and event length.
As you schedule your event, keep in mind your event budget and your audience. Consider questions like:

How many hours or days of content can I provide with my budget?
For a given ticket price, how long does the event need to be to justify the cost for attendees and/or sponsors?
Are there times or days that are optimal for my target audience?
Is my audience international or local? What is the best time considering location?
8. Compile a list of event partners.
Will you have external presenters and/or sponsors at your event?

Chances are, your answer is “yes.” If so, brainstorm the ideal speakers and brand partners who might serve your event goals, satisfy your budget requirements, and be an excellent fit your event theme or topic.

Then you can begin targeted outreach (more on that in the next section).

### Securing speakers and sponsors for your virtual event

In-demand speakers are very selective when it comes to the events at which they choose to speak. Similarly, large companies are inundated with thousands of requests for sponsorship.

It’s an understatement to say getting their attention is difficult. 

To add to the challenge, you and your team will need to reach out to dozens of potential presenters and sponsors before filling all of your speaking and sponsorship slots.

Virtual events make it easier to attract top-notch speakers and high-quality sponsors because they remove or reduce the friction of travel, scheduling, and other restrictions of an in-person event.

Nonetheless, you still need to catch the attention of and make a good impression on potential partners to secure them for your event. 

That’s why you may want to create outreach email templates that allow you to save time (since you may be contacting dozens of potential candidates), but which you can customize for each presenter or sponsor.

To write a persuasive outreach email, clearly state the value to your invitee and provide top-line information about your event:

Introduce yourself, your organization, and the event. Consider describing your organization's vision and/or mission, and your event goal to inspire your invitee.
Explain the opportunity, including audience size and type (i.e., the personas your event is targeting), other marquee brands and presenters participating, and how the opportunity can provide tangible value to your invitee.
Consider attaching a speaker or sponsor prospectus. This document shares more details and data about your event.
Talk about your track record. If you have a proven history of running successful events with high-caliber speakers and attendees, mention that in your outreach email. This will instill confidence that your organization will run a professional event.
Share social proof. Include testimonials from speakers, sponsors, or attendees from prior events.
Provide a call to action. Let your invitee know their next step if they want to be involved. The CTA could be as simple as replying to your email.
Remember to keep your message concise. Don't bombard your invitee with a long essay or dense blocks of text. Instead, try to make your ask in a few short paragraphs.

### Choosing a virtual event platform

An event platform can make or break your event.

After spending months planning your event and investing thousands of dollars, the last thing you (or your speakers, sponsors, and attendees) want are technical glitches and mistakes. They make you and your brand look bad.

Meeting and webinar tools are not built to accommodate the scale of or provide the control required for a full-fledged virtual event. Instead, consider a virtual event platform.

A virtual event platform allows you to build a single, branded event destination—with multiple virtual spaces for different types of content and engagement. It is the online equivalent of a physical event venue.

Compared to other video tools, a dedicated online event platform can:

Accommodate vast audiences—potentially hundreds of thousands of people—without hiccups in performance.
Unlock an interactive experience that goes above and beyond the typical webinar or video conference.
Replicate the community and networking opportunities of an in-person event, allowing attendees to foster meaningful connections with fellow attendees.
Enable you to fully customize a virtual venue to produce a branded experience.
Just as it is important to invest in a dedicated virtual event platform, it is also crucial to do the research to find the right virtual event platform for your needs.

Here are some key considerations when shopping for an online event platform:

Reliability: Choose an event platform that’s stable and isn't buggy.
Scale: Consider how many concurrent attendees you expect at your event and ensure your virtual event platform can support that number. 
Ease of use: Ask yourself whether speakers, sponsors, and attendees will be able to easily navigate the platform.
Interactivity: Interaction among attendees and between attendees and speakers is among the most important parts of a virtual event. To enable participation, seek out a platform that includes engagement features, such as one-on-one video networking, small-group video meetings, group chat, direct messaging, the ability for attendees to join live event sessions, etc.
Sponsor experience: If you have sponsors for your event, prioritize virtual event platforms that create a great experience for sponsors—via features like customizable branding and dedicated virtual space.
Branding: Pick a platform that lets you craft a branded environment and an experience suited to your specific needs.
Cost: Find a virtual event platform that fits in your event budget. Consider different pricing models and how they might impact you. For instance, some event platforms charge a flat rate, while others charge based on the number of attendees.
Support: A great event platform will have a technical support team who can help in case anything goes wrong before, during, or after your event. 
Video production: Consider how you want to produce your event video content and see what different options event platforms give you. You may want to consider using a live stream option that allows you to create branded video content.

### Setting up your virtual event

Once you’ve chosen an event platform, you can begin building your online event.

This means customizing your virtual venue, adding session and speaker information, and setting up your registration and/or ticketing process.

Tip: When setting up your registration form, you can ask for additional information that will help you create more engaging event content and give your sales team vital data.

For example, you can ask attendees for general information—like their professional role or company size—which sales can use to customize follow-up emails to lead. Or, you can ask more specific questions—like what topics attendees are most interested in learning about—which you can use to customize your event sessions and content.

Don’t forget to thoroughly test your virtual event platform. Ask your event team or colleagues to test your event with you to ensure everything works as intended.

If you’re working with sponsors, consider scheduling times for walk-throughs and training sessions to teach them how to navigate the event and set up their sponsor booths (if they have one).

After testing, reviewing, and refining your virtual venue, you can publish your event, so attendees can begin registering.

### Promoting your virtual event
Once your virtual event is live, you can begin sharing it with the world. In this section, we provide five ways to promote your virtual event:

1. Social media marketing
Select your channels and create posts that generate attention and underscore value.

In your posts, emphasize what attendees will learn and spotlight any high-profile or interesting speakers who will join.

For instance, this tweet promoting StreamYard’s Live Stream Success Summit features best-selling author Seth Godin. The post also tags Godin, notifying him of the post and giving him the opportunity to re-share it to his own audience.



Virtual event promotion social media


Create images sized for the different social media platforms where you plan to promote your event. You can use this handy guide to social media image sizes from SproutSocial, as a guide.

2. Email marketing
Email marketing is one of the most effective ways to increase registrations for your event.

In fact, 83% of marketers use email marketing to promote content like events—and, on average, people earn $42 for every one dollar they spend on email marketing.

To take advantage of how effective email marketing is, you may want to send a series of emails leading up to your event. In your emails, you can:

Announce speakers and tout their expertise.
Promote event sessions, such as keynotes and panels.
Emphasize key takeaways and value for attendees.
Highlight the different networking opportunities at your event.
Tease parties, DJs, yoga sessions, and other social activities and entertainment.
Share social proof from prior events.
Use urgency to drive registrations (e.g., offer time-bound discounts on ticket prices).
As an example, ProductCon, a conference for professionals who work on tech products, promoted their conference in their digest newsletter.

Virtual conference promotion email


3. Website marketing
If your website attracts regular traffic, it's a powerful place to promote your event.

Here are four ways to market your event on your website:

Add promotional banners and forms to relevant pages on your site.
Write an announcement blog post that teases your virtual event.
Create a promotional video and publish it on your site.
Design a home page hero banner (i.e., the topmost section of your homepage).
Womentech Network, a professional community for women working in technology, promotes their annual Women In Tech Global Conference on the hero section of their home page.

homepage hero banner promoting virtual event


4. Referral marketing
Why rely on your audience alone? With referral marketing, you can incentivize other companies and individuals to promote your event by giving them a percentage of ticket sales or rewards.

You can even give companies in your referral program marketing assets to more easily promote your event. For instance, you can send them a promotional kit that includes social media banners, email marketing banners, and promotional swipe copy.

Tip: With the Hopin virtual event platform, you can create a referral program for your event and track all referrals.

5. Co-marketing
Similar to referral marketing, co-marketing promotes your event to people outside your own audience.

The key difference: Instead of paying others to share your event via a referral program, you partner with like-minded companies who agree to share your virtual event for free.

Typically, you’ll offer something in exchange for this promotion. For instance, you might agree to promote your partners’ events in exchange for them promoting yours. Or you might offer them a free sponsor spot at your event.

Co-marketing partnerships are an excellent way for companies to work together to reach a larger audience.

### During your event: How to produce a virtual event

Live virtual events can be nerve-wracking—especially large-scale events with multiple stages and sessions running simultaneously.

In this section, we’ll show you how to confidently execute your event and create a great start-to-finish experience for attendees, speakers, and sponsors.

Creating professional live and pre-recorded video content
When it comes to your program, the first question to ask is whether you will use live video, pre-recorded video, or a mix of both.

Both live video and pre-recorded video have their advantages and disadvantages.

Live video is often more interactive and engaging. Indeed, it allows for presenters to respond to audience feedback in real time, transforming the audience's role from spectators to active participants.

Of course, live video can be risky. If something goes wrong (like your speaker losing internet connection), your attendees will know right away, and it can create a bad impression.

Pre-recorded video can often have greater production value and can be more reliable and safe than live video. However, it lacks the interactivity, urgency, and authenticity of live video content.

On top of that, pre-recorded video invites the question, “Why should I attend your live event if all of the video is pre-recorded? Can't I just watch it on my own time?”.

That’s why we recommend a mix of live and pre-recorded video content.

No matter what you choose, you can elevate the quality of your video content and by incorporating a few simple elements:

Title cards or title rolls: An image or pre-recorded video at the beginning of a session that displays the session title and any CTA.
Lower thirds: A lower third is a combination of text and graphical elements placed in the lower area of your video—like speaker names or topic cards. It can be displayed throughout the entirety of a live stream or during certain moments.
Event branding: You can add your event colors and logo to all video content to provide continuity and a professional, produced quality. 
Backgrounds: This is a graphic or backdrop that appears or is displayed behind your speakers, hosts, and sponsors. Typically, it’s branded with your event colors and logo. It removes the distraction of busy backgrounds and adds consistency to your event. 
Here’s a great example of a branded live video at the StreamYard Live Stream Success Summit:

branded streamyard live video virtual event


Key takeaways from this example:

It features a branded background with the StreamYard brand colors and eye-catching, textured graphics.
It uses lower thirds—speaker names as well as a banner prompting attendees to ask questions.
Bonus: This video even includes a live sign language interpreter—on the right side of screen.
Tip: Using StreamYard, you can easily live stream branded, produced live video content within a Hopin event.


Driving audience participation in real time
For many attendees, interaction is the difference between an engaging virtual event and a boring one.

One of the primary advantages of an online event is that it opens up a two-way dialogue between presenters and attendees—no matter where they are around the globe.

A key to providing great interaction and engagement is a platform that allows remote participants to share ideas, questions, and feedback via video, chat, polls, and other engagement levers.

Once you have a platform that facilitates audience participation, you need a team of moderators who directly communicate with participants; share their questions, ideas, and feedback with speakers; and increase the interaction with human guidance.

Moderators facilitate interaction and increase engagement at virtual events by:

Sharing key attendee questions and prompting speakers to answer those questions. This opens a two-way dialogue between speakers and attendees, making attendees participants in the event, rather than mere spectators.
Inviting attendees to join the live video to ask questions. This is a step above chat. It allows attendees to meet speakers and talk to them face-to-face. It gives presenters the opportunity to hear the tone of questions and to see whether or not attendees understand their answers.
Watching out for negative or inappropriate comments and stopping trolls from disrupting the live event and forming negative opinions. If an attendee is acting inappropriately, a moderator may remove them from the event entirely. If they are leaving negative feedback in chat, a moderator may just respond to their comment and try to resolve the issue.
Communicating important messages like what attendees should do next, what’s going on at the virtual event in the moment, and instructions for participating. Moderators can communicate through chat or pinned chat messages, which are highlighted at the top of chat feeds. Or they can communicate via live video.
Communicating with speakers, sponsors, and partners
Since moderators may be busy during the event, you should consider creating a communications team dedicated to handling interaction with speakers, sponsors, and partners.

Your communications team can answer questions; remind partners of the schedule; and ensure your speakers, sponsors, and partners have all the information and guidance they need to be successful.

Managing and troubleshooting technical issues
Unfortunately, regardless of how well you plan and set up your event, technical issues can still occur during your live event. For these situations, you should have a technical support team who can resolve any problems.

Here are the issues your technical support team will watch out for and help to fix:

Event platform glitches and bugs
User error by speakers, attendees, and sponsors
Internet connection and browser problems
Slow internet speed and lag 
In some of these situations, your technical support team will be able to resolve the issue themselves. At other times, your technical team will need to guide an attendee, speaker, or sponsor to a resolution and communicate how to troubleshoot.

Sending reminder emails
You can increase attendance and engagement by sending reminder emails throughout your event to encourage event registrants to join and participate.

Because these reminder emails reach registrants in their inbox, they can see them whether or not they are actively inside your event venue. With a multi-day event, this is especially crucial as attendees will likely leave and rejoin the event venue each day.

As an example, Ladies Who Launch, a virtual summit for female founders, used Hopin’s email marketing tools to send out simple reminders during their event.

virtual event reminder email


Sharing insights and updates on social media
Don’t let email marketing hog all the glory. Social media is another great way to update attendees (and others) during your event.

You can post speaker insights, key takeaways, and reminders on your social media platforms to encourage attendees to engage on social media and to increase awareness for and interaction at your event.

For instance, virtual whiteboard platform Miro posted on social media throughout their virtual event Distributed. At the end of each day, Miro previewed the next days' programming. Posts like this remind registrants to attend and reiterate the value of doing so.

virtual event social media reminder


You can also share social media posts from attendees—whether they’re posting about insights from the event or giving positive feedback. This can serve as social proof and grow brand awareness.

‍

### After your event: How to measure success and drive more value

Alas, your work isn’t done when your virtual event wraps.

Rather, this is when you follow up with event participants, review your results, repurpose your event content, and learn and optimize.

In this section, you’ll get a post-event action plan to walk you through what to do after your virtual event ends.

Following up with attendees, speakers, and sponsors
Using email marketing, you should follow up with event participants after your event to share any information, including next steps and calls to action.

Your call to action should tie back to the event goals you established in the first section of this Guide. For example, if you’d like to increase product sales, send attendees an email with a call to action to try or buy your product.

Here are different content ideas for post-event email outreach to attendees, speakers, and sponsors:

Attendee email outreach: You can email your attendees feedback surveys, replays, additional resources, and CTAs to register for another virtual event or try your product or a sponsor product.
Speaker email outreach: Email speakers to thank them for joining your event and send them speaker feedback surveys, a virtual thank-you gift, and/or additional resources.
Sponsor email outreach: Thank sponsors for participating and send them lead info (if applicable) and event performance data, like booth visits and total attendance (more on this in the next section).
Measuring, analyzing, and improving using event performance data
Many event organizers fail to set aside time for an in-depth post-mortem, especially reviewing event data.

Yet this is one of the most vital ways to consistently improve your virtual events—indeed, your overall marketing—and it's one of the major advantages of a virtual event.

A good virtual event platform can provide valuable insights that an in-person event can't—such as data showing what event content your attendees consumed and for how long, how many private meetings they participated in, and lots more.

Since a virtual event platform can reveal, for instance, what content drove the greatest audience engagement, it can inform not only your future events, but can also be a valuable source of insight for your broader marketing—even your overall business strategy.

For example, if you find that a particular topic attracted the majority of attendees for long periods of time, you can create content marketing campaigns around that topic and feel confident your audience will be interested in the campaign.

With the right data in hand, you can measure and optimize nearly every aspect of your event.

For example, if your attendance rate is low or below your historical average, you can survey your attendees to find out why they registered but didn’t attend. With the findings from this survey, you can iterate and improve for next time.

Here are some key metrics you should analyze and review after every online event:

Ticket sales: Look at your total ticket sales and compare it against your initial goal. This will help you measure the profitability of your virtual event.
Sponsor revenue: Examine your total sponsorship revenue. Again, this will help you track the profitability and revenue from your event.
Registration: See if you hit your goals for total registrants and look at ways to improve your promotion efforts for next time. If you sell tickets, total registration impacts your sales. If your event is free, registration impacts awareness and other brand goals.
Attendance: Look at how many people attended and how long they attended your event. Also, find the most popular sessions with attendees. Higher attendance rates impress sponsors and help you retain and attract sponsors for future events.
Booth interaction: Track who enters an expo booth and find out if they interacted with the booth. Again, this metric matters to sponsors. You can include booth interaction data on sponsor prospectuses for future events.
Attendee engagement and interaction: See poll results, networking participation, total chat messages, connections made, and videos calls hosted. This will help you understand how engaging your event content was.
Attendee feedback: Examine attendee feedback from your event. You can gather attendee feedback with an NPS score survey, a lengthier survey, or both. This will help you understand how attendees perceived your event and what they liked and disliked.
Tip: Hopin automatically shows attendees an NPS survey at the end of every event. This makes it easy to gather feedback. 

Related: How Celonis Used Hopin to Track Event Performance

Repurposing virtual event content to maximize visibility and engagement
After spending weeks and maybe months planning and producing your event content, don’t let it hide unnoticed on your website or in a replay email. Instead, repurpose your event content to share the insights in new places or ways.

You can repackage event content into blog posts, videos, podcast episodes, ads, social media posts, guides, and books. You can even leverage long-form video from the event as gated on-demand content, to generate leads long after the event is over.

Imagine you hosted a conference for hotdog fans. A few speakers talked about the best tips for cooking delicious hotdogs, so you write a blog post that includes:

The speakers’ tips on cooking hotdogs
The entire video from their talk or clips from it
Screenshots or images from their talk
The speakers’ names and links back to their site
Once you’ve published the post, you can ask speakers to share it with their audiences, you can post it on your social channels and promote it with paid spend, you can include it in email newsletters, and so on.

Repurposing your event content is a powerful way to drive even more value from your event once it’s over.


### Conclusion: Three Principles of Hosting a Virtual Event
With a professional online event, you can build awareness and affinity for your brand, educate and engage your audience, nurture community and connection, generate business leads and direct revenue, and more.

Best of all, virtual events unlock a new ROI framework—allowing you to drive down event costs (e.g., physical venue, travel, accommodations), while creating new return in the form of increased audience size; broader, global participation; and access to event analytics.

While virtual events have significant advantages, it takes the correct strategy to realize those advantages. Use this Guide when you are considering hosting an online event to make sure that you are taking into account all the important variables that will determine your event’s success.

Above all, keep in mind these three principles:

Don’t start planning or preparing until you know your goal(s)
Always keep your audience in mind and program content that creates value for them
Remember to prioritize engagement and allow your audience to shape the conversation

## Gamification and Other Ways to Increase Engagement

