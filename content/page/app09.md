---
title: Fitness suggestor
subtitle: Move reminder, TABATA suggestor, WOD suggestor
comments: false
---

***WARNING: The first RULE of every GYG is to first look around and then critically review/revise or even completely throw out the following background for a GYG ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

Like [GYG05, the time mgmt app](https://markbruns.gitlab.io/gygbe/page/app05/), except that exercise not only NEEDS to happen ... it should be SMART exercise, or the specific kind of exercise that would do you the most good, right now ... not what you want to, because maybe you're great at that, but what you should do, because that area needs work ... so YOU have to drive YOUR exercise app, because YOU also drive YOUR excuses ... YOU must overcome the bad habits of being YOURSELF ... it can't be somebody else's program; it has to be YOUR program, even if you have stolen every last part of it from someone else.

The KEY to exercise is in how you overcoming your own excuse factory [which has benefits for everything] ... for example, some folks have found that when you THINK that you are completely gassed and really want to quit, you are only about 40% exhaustion -- the point of REAL exhaustion is 2.5X further out there.  So, knowing this, they push on and JUST KEEP GOING. Muhammad Ali was famous for saying that he didn't really know how many repititions he did, he just knew that when he thought he was done, he would do ten more ... and before he got to that first ten, he'd find that he'd have a new burst of energy and he would push on not counting again until he thought he was done, and then do ten more.

Exercise is about overcoming your own excuse factory ... for EVERYTHING ... it's the discipline-building parts of getting after your weakest area of fitness ... and you should do that throughout the day ... rather than telling yourself "Good Workout" and then SITTING for the rest of the day.  Different people will find that they must go to different places in their head in order to find the different things that get them past their excuse factory ... but a lot of those little mental tricks will be some sort of variation on telling oneself [when one thinks one is done] something like *just one more set* or *just ten more reps* and that transfers into *one more cold call* after one is tired of selling or *ten more words* after one can't think of another word to write.

# DISCIPLINE = FREEDOM 

The reason to exercise physically ... 4 minute TABATAs throughout the day -- punctuated by 25 minutes of work and two 30s spaces of re-set/re-group ... is to build the mental DISCIPLINE of excercising for an intense 4 minutes AND focusing intensely for just 25 minutes each half hour ... the work on building discipline must be relentless, continual and, above all else forgiving.  You should not forget those times when you let up, but ***SO MOTHERFUCKING WHAT*** ... you have to FORGIVE YOURSELF in order to build discipline. *If you blew it in the last 30 minutes or the 30 minutes before that -- it does not really matter; the 30 minutes that are coming up are the ones which matter.*

DISCIPLINE = FREEDOM

## Get up. Stop Sitting. Walk and Think.

Nothing drops blood glucose levels and lessens the likelihood of problems with insulin resistance that just NOT SITTING ... in a nutshell, nothing does a better job of burning off excess blood glucose than using muscles to bear body weight ... sitting also drives osteoporosis and the *cute, little* posture and gait of old people. The human skeleton and muscle structure was clearing *designed* for humans to be standing whenever they were not laying flat ... the chair or, worse, the vehicle seat has turned people into unfit, constant stress carriers, perpetuators and amplifiers [because humans do not work off stress as eustress that stress *ponds* in the system that circulates the cortisol] ... humans now don't have a cardiovascular system, because of sitting and failing to burn of the cortisol -- they have a cortisol, insulin and blood sugar circulating system that still delivers just enough oxygen so that ***people don't even realize that they are actually dead.***

## Recognize the EXCUSE factory
🏴‍☠️
“I gained weight because of an injury.” OY!! There are roughly zero calories in an injury so let’s all agree that, that statement just isn’t true. You got felt sorry for yourself and you weakened the discipline by making poor choices because the excuse factory kicked ... you gained a little weight. then you gow way out of shape. One poor choice led to another when you don't work on discipline, discipline atrophies ... then, feeling defeated, you made things worse and then still worse and denied that you HAD to start working on discipline, so the cycle continued ... so now, welcome to the MASSIVE challenge you created by avoiding the need to work at discipline along the way.

DISCIPLINE required daily, if not hourly maintenance ... stop denying that DISCIPLINE = FREEDOM.