---
title: About GYGbe
subtitle: GYGs are laboratories for exploratory self-learning; the objective of each lab is a tool that one uses for professional growth.
comments: false
---


***WARNING: The first RULE of every GYG is to first look around and then critically review/revise or even completely throw out the following background for a GYG ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

The point of GYG is exploratory learn ... GYG is a cloneable, forkable set of learning labortories which you build/extend yourself.  The objective of each to learn how to configure and build the tools that will help you use dataflow and gather intell.  The ultimate goal will always be about [building a better SUCCESS BOT](https://markbruns.gitlab.io/gygbe/page/app12/).

GYGs will ALWAYS be a work-in-progress ... the whole endeavor is a messy, living, changing language used by DOERS ... of course, it will never be complete -- it will also be especially half-baked, under deep, deep construction. 

# What else would you expect from an exploratory learning-as-code project named Git Your Gyg.

As individuals, we work for the sake of doing better, more creative, more fulfilling work ... we should *see* unforeseeable growth in ourselves and in our network of friends in five years ... we should *see* the kind of growth that would be almost unrecognizable, unfathomable to all of us right now. The ONE constant is that the DISCIPLINE of learning continually will always give us greater capacities, greater freedom.  #DISCIPLINEequalsFREEDOM

We can develop open source applications that reflect our art or life's work and particulary matter to us ... OR we can do work for those who pay us to work on specific projects.

If GYGbe is at all different from other intelligence-gathering tools, knowledge-engineering or productivity toolkits, it is because GYBbe is extensible open source abolitionist activism -- it's not about BEING perfect, it's about perfecting the code of doing one's life work. 

As such, GYGbe, involves the recursively [dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) of EVERYTHING about the intelligence-gathering knowledge-engineering applications that we think about in work with the use of clonable, version-controlled, exemplary Git-best-practice repositories that are in the process of becoming more perfect. We also use the services of other toolkits and service providers -- we particularly value the entire gamut of evolving [best continuous integration and continuous delivery practices of Gitlab](https://docs.gitlab.com/ee/ci/introduction/), for example.

This means that YOU can GIT your own GYG by cloning the GYG repositories and making GYG your own ... GYG is necessarily about INTENSELY PERSONAL liberation activism; abolish slavery in your own life first, then abolish slavery for others.  Since GYG repositories are FREE, extensible, clonable -- GYGbe is necessary about open sourcing the tools of modern abolition ... abolition is made possible through skills and capabilities. 

As Viktor Frankl's example reminds us -- EVERY human always has the freedom to make the best of whatever circumstances one find oneself in. This is why CAPABLE people are always more FREE, more dangerous, enjoy more liberties than thugs who lack skills and capabilities. ALWAYS. As FREE human beings, we understand why we cannot be truly free until OTHER humans are also free -- but we cannot force that with coercive policy. People must voluntarily choose freedom and the responsibilities of freedom. No humans can ever possibly be sustainably free if that human lacks capabilities and skills which others value and accordingly is at the mercy of experts, caregivers, bureaucrats or prison gaurds. 

Ending slavery is what abolitionism is about; true abolitionism is NECESSARILY distributed -- but also NECESSARILY completely free of coercion.

Freedom enables growth; people can use their freedom for whatever they want to use it for, even self-destruction, but people should use their freedom to build health, build the capacity for growth. Thus, the GYGbe long-term objective is healthly growth ... we grow stronger, tougher, more capable through antifragile sustainability.  We do this because we care about the marvelous Creation we inhabit -- we grow our capabilities for our Universe, for our planet, for our fellow humans, for ourselves.

Successful individuals enjoy working ... NOT for $, fame, power, status ... but working for the sake of ONLY their work, for ONLY their life's calling.  Successful individuals do not seek distractions or inebriation -- successful individuals do not really enjoy ANY kind of prolonged absence from their life's work.

We seek knowledge and gather intelligence, to learn how can be of better service tomorrow than we can be today ... we find learning, planning, implementing, monitoring, adjusting, polishing, simplifying, sorting, refactoring, standardizing to be sustainably enjoyable ... mechanization, automation and artificial intelligence enable us to think more, to work at being more flexible and disciplined, to be able to LEARN how to to those things that will help us profit from greater uncertainty and chaos. 

## Developing Knowledge-Driven Intelligence

1. [Healthy, Reliable, Maintainable, Scalable FREEDOM](https://markbruns.gitlab.io/gygbe/page/ch01)

First we must know our WHY ... why do we want to developing knowledge-driven intelligence-gathering learning software?  History tells us that complacency is never an option -- most wars throughout history are actually the result of intelligence-gathering failures and willful ignorance of constantly changing power dynamic. In other words, tyrants will always use ignorance and the desire for the *easy button, for comfort, for *peace* to dupe, herd and control the masses ... freedom is spectacularly beautiful and invigorating, but it is also scary and challenging.  Freedom is never free and it's not just some sort of entitlement one is born into. Freedom NEVER just happens for people who keep their head down and show up at the plantation every day. Tyrants will always use fear to take freedom and power from masses who just don't want to fight -- but power is NEVER EVER just given back; power must be TAKEN. 

Wars have become incessant continual wars of information and ceaseless propaganda and counter-propaganda.  People like to imagine that they are well-informed if they are well-read or if they have passively watched some type of content that represents some unique, inside knowledge, special truth ... it's IMPOSSIBLE to be well-informed if one only passively consumes content; being well-informed means networking with people [around the globe], constantly curated sources of different forms of intelligence and time spent actually DOING relatively hard things and LEARNING new skills.  

Some things are easier now; some things are much harder.  The constant throughout History has been change and the fact that right now the unknown unknown parasites and viral infections of every form, including human and spiritual are seeking ways to exploit our vulnerablilities.  Physical health is, of course, fundamental as an example for how we must continually build discipline, resolve and readiness -- ceding all responsibility for physical health "to the experts" will land one in a drug-addled situation where one is under locked-down expertcare. 

Information and intelligence gathering technology is also too important to be left to the specialized experts.  The life- and mind-changing content that gave us the Enlightment and the revolutions that toppled autocrats has been moving from only the printed word to streaming audio/video and then on to computer-enhanced interactive data-driven VR-enabled data visualizations. Applications have been moving toward a world that is exponentially more data-intensive, as opposed to compute-intensive but even compute-intensive AI/ML simulations continue to tap the still expanding resource of cheaper, more power-efficient, task-specific CPU power.  

2. [Languages, DataAPIs, AI/ML Frameworks](https://markbruns.gitlab.io/gygbe/page/ch02.html)

Programming languages [including query languages], data models and APIs, AI/ML frameworks are likely to be the most important part of developing knowledge-driven intelligence-gathering learning software because these have such a profound effect how we even BEGIN begin to formulate ideas and think about the problem that we are solving.  The limits of languages and data models limit the realm of software ... in good ways, perhaps, but also in ways that we don't even realize because we don't have a way to even express what is missing.

3. [Storage and Retrieval](https://markbruns.gitlab.io/gygbe/page/ch03.html)

A database needs to do ONLY two things. When you give it some data, it should store that data. When you ask it again later, it should give the same data back to you. This is not a trivial task given the insecure, fault-prone internet environment in which the database must operate and the number to times that your applications will ask it to perform these two things.

4. [Encoding and Evolution](https://markbruns.gitlab.io/gygbe/page/ch04.html)

Applications MUST inevitably change, but change in some sort of predictable compatible way over time. Features are added or modified as new products are launched, user requirements change and become worse or hopefully better understood and business circumstances change in a completely unpredictable manner ... the user does not care about any of that upgrade or change stuff, unless they requested a feature that they think the application should already have and that feature never shows up.

5. [Replication](https://markbruns.gitlab.io/gygbe/page/ch05.html)

Replication means keeping a copy of the same data on multiple machines that are connected via a network ... geographically to reduce latency ... redundantly to provide fault-tolerance ... scalably to handle increase user participation and demands for throughput.

6. [Partitioning](https://markbruns.gitlab.io/gygbe/page/ch06.html)

For very large datasets, or very high query throughput, that is not sufficient: we need to break the data up into partitions, also known as sharding.

7. [Transactions](https://markbruns.gitlab.io/gygbe/page/ch07.html)

In the harsh reality of data systems, many things can go wrong ... the user does not care about any of that and is completely impatient and unforgiving about it; users only care that your application never threatens the integrity of THEIR transactions.

8. [The Trouble with Distributed Systems](https://markbruns.gitlab.io/gygbe/page/ch08.html)
Anything that can go wrong must go wrong -- it's necessary to have your shit tight, because you must also assume that you don't yet realize all of the combinatorics of everything in the environment in which system runs ... so it's as if stuff [you thought handled by others beyond your control] that wasn't supposed to be able to go wrong will go wrong. At the worst possible time.  

9. [Consistency and Consensus](https://markbruns.gitlab.io/gygbe/page/ch09.html)

The simplest way of handling catastrophic faults is to simply let the entire service fail, and show the user an error message. If that solution is unacceptable, we need to find ways of tolerating faults—that is, of keeping the service functioning correctly, even if some internal component is faulty. This chapter explores SOME examples of algorithms and protocols for building fault-tolerant distributed systems.

10. [Batch Processing](https://markbruns.gitlab.io/gygbe/page/ch10.html)

A system cannot be successful if it is too strongly influenced by a single person. Once the initial design is complete and fairly robust, the real test begins as people with many different viewpoints undertake their own experiments.  In online systems, eg, a web browser/app requesting a page or a service calling a remote API, the request is typically assumed to have been triggered by a human user and the user is waiting for the response. The web browser/app, and increasing numbers of HTTP/REST-based APIs, have made the request/response style of interaction so common that it’s easy to take it for granted, but there are other approaches have their merits ... online services ... offline batch processing ... streaming near-real-time systems.  It might seem *old*, but batch processing is still an extremely important building block the overall desire for reliable, scalable, and maintainable applications ... but more and better data, eg video/audio ... and that also means more and better uses for data, ie, not *old* as much as permanent and still changine and growing.

11. [Stream Processing](https://markbruns.gitlab.io/gygbe/page/ch11.html)

In reality, a lot of data is unbounded because it arrives gradually over time: your users produced some data yesterday and more today, and they will continue to produce much more data tomorrow. Unless you go out of business, this process never ends, and so the dataset is never “complete” in any meaningful way. In general, a “stream” refers to data that is incrementally made available over time. This chapter looks at event streams, eg audio, video, and  how they are treat as a data management mechanism: the unbounded, incrementally processed counterpart to the batch data we saw in the last chapter. How streams are represented, stored, and transmitted over a network is discussed first. Then, the relationship between streams and databases is investigated. And finally, approaches and tools for processing those streams continually, and ways that they can be used to build applications are explored.

12. [The Future of Data Systems](https://markbruns.gitlab.io/gygbe/page/ch12.html)
This is not a prediction, but a more of a discussion or how things should be ... fortunately, there are a lot of readers of this influential book ... and through their impact, there's a good chancet that what should happen will happen. 