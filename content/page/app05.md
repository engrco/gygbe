---
title: Time/exercise/calendar mgmt for job seekers
subtitle: USE -- do not waste -- time for other things
comments: false
---

# Speed is power

FOCUS your acceleration of speed upon the speed those routine things which you do OFTEN ... REMOVE all uncessary steps; then aggressively automate the necessary steps with something akin to a [CI/CD build pipeline](https://docs.gitlab.com/ee/ci/pipelines/). 

When we say that, "speed is power" ... we mean that our POWER comes from THINKING about focusing upon value first and cutting everything nonessential out.  Use D.O.W.N.T.I.M.E. to see waste that can be removed. 

AFTER we apply automation, we THINKING about automation ... but it is LEAN that gives speed its focus, its ability to be on target, its ability focus force in a tiny area to give POWER.

As with health, it's not what you add or swallow to *supercharge your engine* ... SPEED is not as much about horsepower as it is about agility and lightness. Speed and focused power come from what you REMOVE, what you take away and then making good habits either involuntary and just part of what you do without thinking OR automating tasks for flawless execution and removal of errors ... SPEED is about what you REMOVE ... REMOVE the waste, remove the errors.

There's no way to overstate the importances of AGGRESSIVELY minimizing and removing the unnecessary ... don't just minimize your purchases -- stop EVERY going into stores to shop; unless there's no way out, order exactly what you want online when you have looked at different alternatives in not just products, but sources ... get rid of commuting, get rid of jobs that force you to commute ... say "ABSOLUTELY NO FUCKING WAY!" to all meeting requests, at least the first time [so that you have fewer future meeting request] ... get rid of relationships that require more effort on your part than what you can ever hope to get back, ie tell people who only call when they need money to get lost ... NEVER EVER EVER endure a friendship with either a gossip or someone who always assumes the role of a victim ... GET RID OF THE DRAGS.   

Optimize the small habits, especially those habits that you will use for the rest of your life ... consider breathwork for example ... make correct breathing part of muscle memory -- every day, devote time to taking thirty correct DEEP breaths ... pay attention to each breath ... then exhale to try to purge your lungs of all air, inhale until you can't inhale any more, hold that breath for 1 min while you focus on relaxing each part of your body... inhale and start again with 30 more correct, DEEP breaths ... then breath in deeply, exhale, inhale, hold that breath for 90s ... then do the final set of 30 breaths, breath in deeply, exhale, inhale, hold breath for 90s or maybe longer.

You can do the same thing with all kinds of OTHER attributes of your PHYSICAL day ... you might work for 25 minutes, do a 4 minute TABATA, then work again, exercise, work, exercise ... the focus of this is no so much about the exercise, but rather it is about improving your health and fitness to better utilize the scarce time in your day ... the focus of speed, repitition, good habits is time management.

OPTIMIZE your value, it's not just about billable hours, but what value you produce during those hours ... since life is unpredictable and you will find yourself in periods of downtime, learn how to use D.O.W.N.T.I.M.E. to see wastes, ie Defects, Overpolishing, Waiting, Not Listening, Transportation, Inventory, Movement for movement's sake, Ego ... don't just set appointments, set the OPTIMAL appointment, eg don't just set up a meeting, set the agenda beforehand including the expectations of exactly what attendees will deliver/discuss -- KEEP the meeting BRIEF; there's no room for prolonged agony -- there's work to do. 

This GYG is not a replacement ... but actually mroe of a calendar optimizer ... it is especially for INDEPENDENTS who work entrepreneurially with others on different side projects ... and perhaps have regular podcasts OR unscripted open-for-freebie-consultuation during brief posted "office hours"