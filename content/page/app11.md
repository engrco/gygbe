---
title: BE USEFUL -- Inventive Problem Solving
subtitle: TRIZ or ARIZ or a DeepMind algorithm that LEARNS???
comments: false
---

***WARNING: The first RULE of every GYG is to first look around and then critically review/revise or even completely throw out the following background for a GYG ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

Edison said that innovation is 99% perspiration, 1% inspiration ... that's why this GYG11 is PARTLY about automating the tedious 99%, eg looking through the last 20 years of patents for similarities / differences to the problem at hand ... but it's mostly about orchestrating the collections of ideas and tinkerers and getting the critical mass to drive the reaction towards something inventive.

We should emphasize that this GYG is not NOT about your imagination or your great patentable ideas ... FUCK imagination!  

GYG is about doing the HARD parts ... imagination is going to happen or it's not ... lazy fucks are never going to turn their imagination into something that helps anyone ... lazy fucks are ONLY going to claim, "I had that idea!" and then wonder why nobody wants to hire them or give them funding.  *FUCK FUCK FUCK imagination!*  ***It's the HARD part, stupid!***

if you have an idea -- the patent process is relatively simple on the scale of challenges in life, ie just patent the damned idea, if you think it's great ... invention and innovation is NOW primarily about puttings things that don't go together TOGETHER ... making new conceptual connections like like this is really about ACCELERATING the logic of learning ... learning is always going to be tough ... except in the realm of exercise and ensuring that our physical abilities don't atrophy, we should not try to learn something that a machine could do better, faster, more reliably, cheaper than we can ... but even with the application of AI/ML, our own inventiveness is NOT really about eliminating the perspiration, it's about making that perspiration moe productive ... invention will still be HARD and tedious -- ***the automation of tedious will expose new layers of tedious.***