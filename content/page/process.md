---
title: Process
subtitle: ENGRco constantly researches, tests, uses, becomes, IS its own open source product.
comments: false
---

[Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) is the practice of an organization using its own product ... we USE the product ourselves to develop it; we necessarily use it long before that product is ready ... we share EVERYTHING, we share too much, we share in order to get feedback and thinker and develop.

The philosophy behind "eating our own dogfood" the VERY FIRST part of what ENGRco does when we look for better ways than the way we are doing something. 

The very first or foundational principle of our dogfooding process is to recognizing that somebody [in the open source world] is probably doing it better and we should look at that approach and possibly implement what someone else is doing ... so the foundation of what we are about is gathering intelligence and automating the sifting, sorting, processing of NEW intelligence ... there is no point whatsoever in reinventing what someone else is doing better.  

This is why ENGRco has a strong predisposition toward open source technologies; the source of all of our technology is FREE to anyone to extend, use, distribute. But we refuse to be religious or dogmatic like some worthless old geezer about this ... we are intent on LEARNING, so we also use proprietary technologies because we cannot afford to be blind to better approaches; we cannot afford not to know about other approaches.

ENGRco is a knowledge engineering organization; that means that we are fundamentally about applying an old school Systems Engineering approach... in order to understand the full Body of Knowledge, it might help for us to think about *traditional* [Systems Engineering](https://en.wikipedia.org/wiki/Systems_engineering) and the additional disciplines from Applied [Complex Systems](https://en.wikipedia.org/wiki/Complex_system). 

## Understanding The Problem BEFORE Pitching Solutions

*In a nutshell, ENGRco is, first of all, before anything else about LISTENING and understanding the problem space.* Nothing. Else. Matters.

At least, 99.999% of what we ever do is HIDDEN.  

It's NOT that it is a proprietary secret ... companies with proprietary secrets are EFFECTIVELY telling you that they don't want anybody to know how incredibly little they know.

We work exclusively in NON-proprietary space ... our "secret sauce" is NO secrets -- but we also do not ever imagine that our knowledge of a problem space is sufficient.  You will not find stupid proprietary gadgets here, because what we do is about ***gathering*** intelligence, RATHER than making the mistake of portraying what we do as being intelligent or full of shiny expertise.  

Gathering intelligence is NEVER finished ... it is always *getting a better picture* ... *drawing NEW maps* and improving the map-making process... sifting, sorting, categorizing, understanding NEW emergent order in the evolving chaos.


- [Category Theory](https://en.wikipedia.org/wiki/Category_theory), [Functors](https://en.wikipedia.org/wiki/Functor), [Natural Transformations](https://en.wikipedia.org/wiki/Natural_transformation), [Algebraic Topology](https://en.wikipedia.org/wiki/Algebraic_topology) and [Homology](https://en.wikipedia.org/wiki/Homology_(mathematics))

- [Graph Theory](https://en.wikipedia.org/wiki/Graph_theory), especially [Dataflow Programming](https://en.wikipedia.org/wiki/Dataflow_programming), eg [Kubeflow](https://en.wikipedia.org/wiki/Kubeflow) or [Directed Acyclic Graphs](https://en.wikipedia.org/wiki/Directed_acyclic_graph) like [Git](https://git-scm.com/book/en/v2)

- [Group Theory](https://en.wikipedia.org/wiki/Group_theory), especially [Lie Groups](https://en.wikipedia.org/wiki/Lie_group), [Lie Algebra](https://en.wikipedia.org/wiki/Lie_algebra)- Nonlinear Dynamics

- [Self-organization](https://en.wikipedia.org/wiki/Self-organization), [Spontaneous Order](https://en.wikipedia.org/wiki/Spontaneous_order), [Social Network Theory](https://en.wikipedia.org/wiki/Social_network), [Semantic Networks](https://en.wikipedia.org/wiki/Semantic_network), [Semantic Parsing](https://en.wikipedia.org/wiki/Semantic_parsing), [Network Analysis](https://en.wikipedia.org/wiki/Social_network_analysis) and [Emergence](https://en.wikipedia.org/wiki/Emergence)



There are  other topics that impact what we look at ... a simple listing is maybe best to get an understanding of what kinds of things [Complex Systems](https://en.wikipedia.org/wiki/Complex_system) might involve: 
- Reductionism and Irreducibility
- Phase Transitions
- Agent-based Models, Networks, and Cellular Automata
- Stability and Instability
- Ergodicity
- Chaos
- Probability
- Statistics
- Central Limit Theorem
- Independence and Interdependence
- Cascades
- Tails
- Stochastic Processes
- Random Walks
- Markov Chains
- Statistical Mechanics
- Cybernetics
- Information Theory
- Constraints
- Variety and Entropy
- Multiscale Variety
- Fractals
- Scaling properties and relations
- Pattern Formation
- Evolution
- Fragility and Antifragility
- Biological Development
- Computation and Formal Systems
- Syntax and Semantics
- Embodiment and Heuristics
- Theoretical Biology
- Autopoiesis and Relational Biology
- Anticipatory systems
- Perception and Affordances
- Clubs, Public Goods, Agency, Human Organizations
- Intelligence and Information Warfare
- Architecture and Construction
- Software Development Engineering and Maintainability
- Systemic Risk and Precaution
- Pandemic Response vs Hysteria
- Policy and Effectiveness
- Decision-making under uncertainty [and uncertainty about the uncertainties]
- Agriculture, Development and Land Use
- Utility Infrastructure and Reliability
- Ecology and Climate
- Localism vs Globalism
- International Trade and Macroeconomics
- Troubleshooting and Innovation Processes
