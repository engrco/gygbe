---
title: Networking Curator
subtitle: StackExchange, Reddit, HackerNews, Twitter
comments: false
---

***WARNING: The first RULE of every GYG is to first look around and then critically review/revise or even completely throw out the following background for a GYG ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

This GYG is all about using data APIs ... both NOW and in the future ... along with some AI/MLops elbow grease to make sense of disparate data streams ... our focus is NOT on the data ... it's on the people behind the data. Yeah, the data matters ... but PEOPLE behind data matter more than any set of data, way more than any single download of a single set of data ... our objective in working with data APIs is to meet, understand PEOPLE.

Some people behind the data APIs are just going to be kids trying to do their job, but they aren't really who we are interested in meeting YET ... others are going to be real DOERs, ie people with repositories, reputations, good answers ... making connections [while avoiding fights] with the providers of thought-provoking, angering, stupid, interesting commentary.



***You cannot fix STUPID ... you MUST ignore stupid ... but when STUPID follows you, you should DESTROY it.***

It is perhaps important to mention something here about kindness ... and being judiciously kind, ie as a way of managing one's time and not being played for a sucker ... this means being exceptionally kind to noobs who are frustrated, unsure of themselves, asking the wrong questions, clearly very much still in heavy learning mode ... but using a distinct lack of kindness to discourage those who are recalcitrant about their insistence on knowing the correct way [even though it is demonstrably false], on refusing to learn or on having closed mind, ie believing that 'the science is settled.' 