---
title: asynch.work is about LEAN
subtitle: Participate fully in the economy, not necessarily as a W2-slave or contract employee
comments: false
---

***WARNING: The first RULE of every GYG is to first look around and then critically review/revise or even completely throw out the following background for a GYG ... FIRST ... LOOK ... AROUND ... somebody might already have a better solution. Steal that idea and build on it.***

# Asynch is E A T I N G the world.

It's not just that it makes workflows bearable by getting rid of WASTE ... ASYNCH is winning because of ASYNCH workflows shorten up the development cycles by removing noise and waste of meetings, especially those with no agenda or an undisciplined manager who holds a meeting that takes more than 30 minutes.  Meetings feed dysfunctional cultures which rely upon meetings and mob-based work-shaming. 

The old line, "Why couldn't this meeting have been handled in email?" is absolutely correct; but email may not provide enough in the way of a disciplined, workflow structure in which issues are automatically tracked and appear in KanBan issue boards ... [EVERY meaningful conversation MUST start with a formally-constructed Issue](https://about.gitlab.com/blog/2016/03/03/start-with-an-issue/) for the same reason that every meeting requires an agenda so that people can prepare and not waste the time of others. The labeled Issue allows for building the existing issue tracking functionality and constructing label-driven issue boards to focus.  

The result of generally pushing people to be more disciplined and ordely in structuring their issues pushes knowledge cultures to more efficient, efficacious dev tools and [constantly improving build pipelines](https://docs.gitlab.com/ee/ci/pipelines/) ... using up the whole clock to hand-off dev from one asynch dev to another.

# Asynch is POST-national ... POST-global ... POST-industrial.  Asynch never requires a break or a let-up ... asynch abolishes the practice of holding critical project chain hostage.

asynch.work is about *LEAN*ing the F up about everything in a workflow ... that includes the whole notion of what a firm is or WHY a firm hires slaves or contractors to toil on its plantation, assembly lines or salt mines.

## Machines are machines. Humans are humans.  

When you grind up humans in industrial, meeting-heavy workflows, you should EXPECT burn-out ... and you shouldn't be surprised when people ***GO POSTAL*** ... or EXIT to be coddled elsewhere by someone who can use the damaged, folded, spindled, mutilated, industrial goods which they now are.

It's akin to, but NOT really like the Analyzulator (GYG02) but it's intended to look more at cultural engineering ... gathering ideas about REMOVING meetings while IMPROVING communication.  

It's not JUST about Git, of course -- it's about optimize your ENTIRE toolchain and ENTIRE workflow for INDEPENDENCE ... asynch.WORK can be Git-centric, eg using GitHub or Gitlab as social media to discuss various topics, eg debate optimizing approach with other knowledgable optimizers ... the objective is developing a knowledgenetwork for LEAN discussions are more productive, more like "sharpening the axe" in order to better, more efficiently, more efficaciously "chop the wood."

**For practical purposes, our implementation of asynch.WORK will rely heavily on everything about the Gitlab workflow.**