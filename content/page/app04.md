---
title: Professional Credentialling Apparatus
subtitle: Gather intell about [emergent] knowledge/skills/certifications, overcome eroding half-life of skills
comments: false
---



Academia is NOW largely irrelevant and will need to either adapt and change ... or else disapper. Most people know this ... the irrelevancy of academia is not really news.

Decades ago, it was already becoming clear that the half-life of a knowledge worker's skill set is maybe something like five years and getting shorter.  On the surface, some fields like reliability engineering and the [design for reliability of semiconductors](https://semiengineering.com/design-for-reliability-2/) might appear to be very similar to the way that things were done decades ago -- except that, of course, in the virtual realm, we now have much better ways of accurately simulating aging, even changing the parameters of how an EDA tool simulates the details of something like [Negative Bias Temperature Instability](https://semiengineering.com/knowledge_centers/low-power/architectural-power-issues/negative-bias-temperature-instability/) ... and the more that we know about those tools ... the more that we should understand SPECIFICALLY where the model might mislead or give us a *bad picture.*  

**Trusting what's probably ok to trust and focusing effort on the RIGHT things to be skeptical about is something that one develops with broad, deep, vast, lengthy EXPERIENCE.**

We really want to FAIL faster, fail better or learn more from each failure and recover faster to fail again.  Our real quest is for accelerated intuition.

We might start by asking how can we learn more, faster from more experienced, more knowledgable people and learn what they know faster, so that we can fail at NEW things, as we develop the overall level of intuition and knowledge for everyone from the new failures, new experiences, new surprises.  ***Obviously, we are going to FAIL -- no new knowledge happens without a tested hypothesis and failure ... it's just that we don't particularly learn anything useful when we fail in a way that somebody could have told us to avoid.***

There have been some adaptations in the realm of knowledge and education ... but there will never be a substitute for serious strategy of PAYING MORE ATTENTION and not imagining that your college degree should be valued by anybody.  It's all about what you are qualified to do and who made that determination ... NOW this effectively means taking control, getting rid of the administrators and *rolling your own* partially-automated, continuously re-calibrated, optimized and mastered professional learning/listening appartus.

You might want to pretend otherwise, but it is NOW professionally necessary ... and you know this yourself ... that we must continually develop some aptitude with new tools, new skills, new frameworks, new languages ... but the only guarantee is that YOUR OWN professional learning/listening apparatus can be made to change and IMPROVE ... as long as you want to remain relevant in your profession.  

Implicitly the first "tool" involves LISTENING, developing your OWN practical set of [intelligence gathering disciplines](https://en.wikipedia.org/wiki/List_of_intelligence_gathering_disciplines) to INDEPENDENTLY inform one's understanding ... you can become a fossil or snapshot of how things worked back in your day if you want.  

Professions CHANGE ... consider how the bloodletters have evolved and almost become respectable -- some people actually DO respect medical doctors, now.

 The FACT of knowledge engineering in professions is that certain tools, frameworks, algorithms, language constructs become part of the jargon, the Body of Knowledge, the topics that one must pass to be given the keys to the ICR.  Some forms of information and marketing hype are either poisonous narrative-driven programming or just temporary shiny flashes in the pan ... professional listening means curating [knowledge graphs](https://en.wikipedia.org/wiki/Knowledge_graph) and developing new networks and relationships for better [humint](https://en.wikipedia.org/wiki/Human_intelligence_(intelligence_gathering)), [sigint](https://en.wikipedia.org/wiki/Signals_intelligence), [imint](https://en.wikipedia.org/wiki/Imagery_intelligence) or [masint](https://en.wikipedia.org/wiki/Measurement_and_signature_intelligence).

This app will rely heavily data APIs ... maybe some AI/ML semantic magic akin to [ConnectPapers](https://www.connectedpapers.com/about) ... but making connections and arguing for the sake of understanding or getting at the truth is necessary ... yes, this means sparring with the providers of thought-provoking, angering, stupid, interesting commentary -- those who actually KNOW what they are talking about always enjoy the sparring, the devil's advocates -- whereas the charlatans are easy to spot, ie they are the ones who get offended, feel that they are not being respected.  Since the interactions and relationships are so important, this app might go beyond simply harvesting data and explore ways to set up virtual confs, self-organized side-bars like barcamps perhaps ... narrow, specific discussions in especially niche-ey topic niches.





