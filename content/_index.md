## USEFUL friends LEARN from USEFUL friends.

We are building Git Your Gyg ... you can join us and be part of the "we" pronoun ... or you can clone whatever you from the [GYG.be repository](https://gitlab.com/MarkBruns/gygbe). GYG will never be about what we offer; GYG will always be about what we are building ... NEXT. We are learning/developing/deploying ML Ops pipelines to gather intelligence. 

GYGs are a series of [dogfooded](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) app projects that YOU build in order to LEARN how to do intelligence gathering for YOU ... it's about getting the intell to be SMARTER ... and that means finding friends who are also GETTING smarter and smarter.

Our approach is geared to delivering superior results to zero-bs organizations that actually understand their business well enough to value results that advance their cause.  


## Recursive Intelligence ... Making NEW Friends in NEW ways.

We gather intelligence in order to better prospect for business opportunities as well as to focus the attention we devote to our continuous skills improvement. We develop and curate relationships in a manner that ensures we understand needs -- we then close, then deliver on very small, tightly-scoped contracts which offer JIT solutions for our clients and the best return for our time for us.

We cannot be and don't necessarily want to be a close friend to everyone. There are eight billion people on the planet ... we can probably only be close friends with 8 or so ... probably only 80-800 will be good friends ... and 8,000 ... or 1 in a million ... will ever really have a chance of really sharing our vision. We don't need wealth, power or fame ... we just need a FEW good friends. 

We try to avoid programmed zombies of all stripes ... we don't antagonize them; we just avoid ... we especially try to stay away from soon-to-be-dead-and-gone organizations which value meetings, the appearance of innovation and generally controlling expertise in silos that are managed by managers managing in order to have a career in managment ... the best managers are always going to be like sherpas; the worst managers are going to believe that they are carrying the company on their pathetically puny shoulders, ie the kind of "important people" who use a snowblower or a snow removal service rather than always looking for ways to get exercise.  

## Gathering intelligence and making NEW friends is primarily about making oneself useful in NEW ways.

Dogfooded GYG projects are about YOU learning AI/ML to use AI/ML to learn AI/ML to use AI/ML ... it's deeply recursive.  Our specific mission is just finding ways to be more useful by helping people be more useful making people more useful.

We gathering professional intelligence and stay in better touch with **NEW developments** in one's professional scientific and technological network ... that is really about making new friends with people who are doing similar things ... we look for USEFUL people who are actually USEFUL and good to have as friends, because they push us to be more useful, to push harder on things like dogfooding their own GYG AI/ML projects, because ...

## USEFUL friends LEARN from USEFUL friends.