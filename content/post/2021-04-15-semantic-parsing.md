---
title: Semantic parsing
subtitle: code generation, machine translation, question answering, ontology induction, automated reasoning 
date: 2021-04-15
tags: ["tool", "discipline"]
---

## Code Generation

Code generation or automatic programming has TRADITIONALLY been a euphemism for programming in a higher-level language than was then available to the programmer.  Increasingly though, frameworks, generative programming and meta-programming of [loosely coupled](https://en.wikipedia.org/wiki/Loose_coupling) components of a framework allows applications which have the ability to treat other programs and other data APIs as their input. 

These applications not only are designed to read, generate, analyze or transform other programs, they effectively even modify themselves while running. It's not just about minimalism and  minimize the number of lines of code to express a solution, in turn improving maintainability and FUTURE development time, although the sparse architecture of such minimalism which allows programs greater flexibility to efficiently handle new situations without recompilation takes a lot of time upfront and might require a lot of simplification and refactoring to minimalize.  The lower-level programming to make things simple is HARD, but the goal is to improve higher-level user-who-programs productivity, the lower-level architecture of elegant minimalism, seriously reusable code and component-based software engineering is fundamentally about a very deep and informed understanding of software architecture quality.

As an example of automatic programming, Kubeflow is a free and open-sourced project designed to make running Machine Learning workflows on Kubernetes clusters simpler and more coordinated. At its core, Kubeflow offers an end-to-end ML stack orchestration toolkit to build on Kubernetes as a way to for users-developers to deploy, scale and manage complex systems. Data scientists and engineers are now able to develop a complete pipeline composed of segmented configuration steps. These segmented steps in Kubeflow are [loosely coupled](https://en.wikipedia.org/wiki/Loose_coupling) components of an ML pipeline allowing pipelines to become easily reusable and modifiable for other jobs. This added flexibility has the potential to save an incalculable amount of labor necessary to develop a new data pipeline for each specific use case. Through this process, Kubeflow aims to simplify Kubernetes deployments while also accounting for future needs of portability and scalability.

Features such as running JupyterHub servers allowing multiple end-users to contribute to a project simultaneously. Detailed management of a project with in-depth monitoring/analyzing feedback into an aggressive contribution of open source cloud-native development of Kubeflow and what is maybe [one of the more vibrant issue boards on Github](https://github.com/kubeflow/kubeflow/issues)... Kubeflow's integration with and extension of Kubernetes has become aggressively more and more seamless. Kubeflow can run everywhere Kubernetes runs ... which, by default means GCP [since K8s/TensorFlow/Kubeflow were original Googprojects], but also K8s is also extremely well-optimized on AWS or Azure and others now ... so Kubeflow components will also run well on prem or as some sort of data pre-screening edge IoT. 


## Machine Translation

Not all words in one language have equivalent words in another language or environment -- most words have more than one meaning depending upon the context of use. "Dumb" [machine translation](https://en.wikipedia.org/wiki/Machine_translation) performs mechanical "Find" and "Replace All" substitution of words in one language for words in another, but that alone might be a first processing step because it produces a good translation because recognition of whole phrases and their closest counterparts in the target language ... this might be needed to be performed multiple times as an intermediate step in order to form a neural network intelligent translation of meaning. 

## Question Answering ... and ASKING

Anything can give an answer ... silence is an answer; any sculptor knows that rocks can offer especially profound wisdom at times ... but the REAL intelligence in AI/ML and semantic parsing comes in the ability to ASK smarter questions.

Traditionally, question answering implementation constructed its answers by querying a STRUCTURED database of knowledge or information ... which effectively guarantees that all of the questions the computer asks are dumb, mechanical, Turing obvious questions, ie, it's JUST information retrieval from a file -- it does not seek to have anything resembling an intelligent interaction with a human, but the computer-as-only-information-retriever is still better at information retrieval than human beings, ie this was already clear ten years ago when Watson beat the best humans in the world at an information retrieval game, ie Jeopardy ... information retrieval or "a good memory" USED TO BE a regarded as a sign of intelligence.  

A much better sign of intelligence is the ability to ask the most pertinent question ... to not simply off information, eg what's the population of North Daktoa ... but, based upon knowledge of user preferences to ASK a probing set of questions about the user's reason for curiosity about North Dakota ... this gets into the realm of SMART AI driven adaptive questioneering ... developing the processes to use information to adapt. 

This depends on more than just retrieving unstructured data efficiently ... and requires more than just a little bit of natural language processing (NLP) to have situational awareness to understand the *language* of the "game" ... it's more than just automatically recording and connecting patterns in the data -- the system has to evolve as it trains itself to be more helpful in an assistive manner ... and to change how effectively the computer reasons over time ... as we have seen in the now famous, implementation of [Alpha Go](https://en.wikipedia.org/wiki/AlphaGo) by Deep Mind. 

## Ontology Induction 

Natural language processing (NLP) is a subfield of linguistics, computer science, and artificial intelligence. Ontology engineering is a field which studies the methods and methodologies for building ontologies, which are formal representations of a set of concepts within a domain and the relationships between those concepts. In a broader sense, this field also includes a knowledge construction of the domain using formal ontology representations such as OWL/RDF. A large-scale representation of abstract concepts such as actions, time, physical objects and beliefs. Thus, applied philosophical ontology is about different facets of conceptual modeling to gain a multifaceted perspective to begin to fully encode of knowledge about specific domains in a complete, truly representative and not just one-sided manipulation. This is not just about a static model or map but also include [executable]reasoning rules that support different algorithims to process that knowledge and recursively induce the "best" way of processing most likely to rapidly converge on the "rigth" answers. Ontology languages are usually declarative languages, are almost always generalizations of frame languages, and are commonly still only based on either first-order logic or on description logic ... but that level of sophistication is changing.

## Automated Reasoning 

Define REASONING.