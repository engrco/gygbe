---
title: The Futurology of Remote Work
subtitle: Thought-provoking, perhaps but begs the questions as to why don't people design their workflows?
date: 2021-06-01
tags: ["tool", "discipline"]
---


## Bob Dylan's Algorithm for Knowing When You are a Success
You get up because you like getting up and enjoy your day working at whatever you want to work on for your own reasons ... then you go to bed satisfied that your day has been a success and that you get to repeat that process tomorrow.

We think that pretty much nails it ... it is an implicitly obvious way to know something that anyone could have come up with. Except that it's a catchy or memorable little turn of phrase that attributed to Bob Dylan [so you can decide whether that attribution makes it better than if you had thought of it on your own, without any help from Bob, ie no wonder Dylan won the Nobel Prize for literature.]

Of course, being a success and satisfied with your day sort implies that you know what is valuable and you know that you have worked hard producing something of value, even if you also know that's how the creative process works and what you produced will probably require some revision and that the entire point of it might be to serve as the "bad idea" that you later reject and discard because your work has provoked you to have a better idea.


## The OBVIOUS things about the future of work

People know more than they they think that they know. When you ask hardworking people questions about things that aren't so emotionally overloaded, like politics or religion or pronoun usage, those people have some pretty [good ideas about where the future of work is PROBABLY heading](https://twitter.com/chris_herd/status/1375429865281875976) ... they might not KNOW for sure, but then again, some of this is kind of obvious, like Dylan on success, when one thinks about it. 

How could it possibly be otherwise?!!

We are not going BACK to how things were before 2020.  It's not like the decades of 1920s or 1950s or 1980s followed the 2000s ... the Cold World War did not look anything remotely like World War I [and its continuation, World War II], just as those world wars did not look anything like Genghis Khan's world wars ... the FUTURE is NEVER EVER EVER about a simplistic return to the past, even though there can be similarities, eg how the 2010s turned out should not come as ANY surprise to someone who was paying ANY attention to things in the 2000s. 

The REALLY big question is "WHY don't people take responsibility for their own workflows?"  One can understand why a clueless fuck would go work at McDonalds or Walmart or Tesla, but WHY IN THE LIVING HELL do SUCCESSFUL capable people insist upon being coddled with a steady paycheck, steady work environment, steady workflow with even their laptops, cellphones and communication devices be provided [and TRACKED] by their employer. Are these people actually INDEPENDENTLY SUCCESSFUL ... or do they need the support of someone else directing their work? 

## Summary

asynch.WORK is only for people who are or aspire to be INDEPENDENTLY successful.
