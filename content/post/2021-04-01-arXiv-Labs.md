---
title: Adapting the arXiv Labs approach
subtitle: Sharing the processes involved in curation of knowledge graphs
date: 2021-04-01
tags: ["tool", "discipline"]
---


## The one who learns the most in any classroom is the one doing the teaching

