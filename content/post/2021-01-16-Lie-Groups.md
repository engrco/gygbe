---
title: Lie Groups in theoretical group theory for nextgen AI/ML teaming algorithms
subtitle: Smooth, continuous, mathematical symmetry groups of applied operators
date: 2021-01-16
tags: ["AI/ML", "Theory"]
comments: false
---

GYG.be is fundamentally about the APPLIED theoretical work in Group Theory ... perhaps an attempt to bring the general notions of brute-force comparison of professional networks ... it's about dating in a way ... using AI/ML to suggest companies, teams, open source repositories, human rights activistis and X-WithoutBorders technical do-gooders ... it's NOT a replacement for making friends -- it's just about using communication technology and AI/ML to do the heavy lifting in the task of networking ideation ... it's better than hanging out at a cafe, at the student union, at a coffee shop hoping against hope for a serindipitous physical meeting.

Unless, anyone has completely missed the point ... physical meetings have RISKS ... the biggest risk is the NEAR CERTAINTY of wasting your time commuting to the type of pointless clusterfritz that always attracts the kind of people who are stupid enough to commute to physical meetings ... and maybe even worse is the RISK of stumbling into the dark hole of complete FANTASY of waiting in fantasyland for other senile people who happen to hang out in fantasyland hoping for a serindipitous meeting.  

The point is ... physical meetings come with giant RISKS ... and we are not talking about silly things like colds or flu or some sort of riot-inducing election-wrecking made-up virus.

***It does not matter -- SMART people find ways to MANAGE risks. Especially when it comes to relationships!***

SMART people change the probabilities in their dice ... because the Game of Life is not remotely CLOSE to being fair ... not all grownups get that -- you'd think it was obvious ... for example, maybe you didn't get into the elite high school or the perfect Ivy League college ... so you can't be an entitled dumbfuck with your silver spoon in the coke, which Daddy's influence peddling paid for -- so you are just another average not-woke-enough hustler -- that means you have to invent your life, you have to tell geography to go fuck itself because you are going USE using existing databases and data APIs and every trick you can find -- then you will troll/lurk like a motherfucker to meet other smart people who ALSO weren't stupid enough to waste their time networking in the best schools.

*But you need to stop whining about how badly fucked you are and how the world is fucked and against ... you must STOP making excuses and start USING data for your ends.*

Relationships are NOW all about suggesting ways to narrow the search for the needle in the haystack ... GYG.be is the application of analogous conceptual thinking to a group theory problem that might not appear to be so analogous on the surface.  One of our areas for study is [Lie Theory](https://en.wikipedia.org/wiki/Lie_theory) or [Lie Algebra](https://en.wikipedia.org/wiki/Lie_algebra) or [Lie Groups](https://en.wikipedia.org/wiki/Lie_group) and [Symmetry Groups](https://en.wikipedia.org/wiki/Symmetry_group) ... the intuition is that we have this similarity of properties in these problems, so what might be a quasi-symmetric or approximately symmetric operation to perform to make the solution more obvious ... given this complex thing, how might we fold/knot/unfold/unknot it to make a solution simpler.