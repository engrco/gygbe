---
title: Large scale data wrangling
subtitle: GCP, AWS, Azure
date: 2021-07-01
tags: ["tool", "discipline"]
---

This is kind of an exloration ... not a dive, certainly not a deep dive, but more of a view from a mile or so up where you can make out the branches of the trees in the forest ... it's about the pipelines, methods, practices which surround the wrangling of really large-scale data ... obviously, when you work on project that use this kind of thing, you will want to know in excruciatingly specific detail how the stuff works.

## [Large scale data wrangling ... the Google approach in a nutshell]](https://cloud.google.com/architecture/data-lifecycle-cloud-platform#processing_large-scale_data)


* Cloud Dataproc [on the GCP]provides you with a managed Hadoop cluster with access to Hadoop-ecosystem tools (e.g. Apache Pig, Hive, and Spark).  People who should be familiar with the now famous [MapReduce paper](https://research.google/pubs/pub62/) and should already familiar with Hadoop tools and have Hadoop jobs

* Cloud Dataflow provides you with a place to run Apache Beam based jobs, on GCP, and you do not need to address common aspects of running jobs on a cluster (e.g. Balancing work, or Scaling the number of workers for a job; by default, this is automatically managed for you, and applies to both batch and streaming) -- this can be very time consuming on other systems. Cloud Dataflow also offers the ability to create jobs based on "templates," which can help simplify common tasks where the differences are parameter values.

* Cloud [Dataprep](https://docs.trifacta.com/display/dp/) is a service for visually exploring, cleaning, combining, and preparing data for downstream analysis. You use Dataprep using a browser based-UI, without writing code; it automatically deploys and manages the resources required to perform the transformations, based on demand. You can transform data of any size stored in CSV, JSON, or relational-table formats; it uses Dataflow to scale automatically and can handle terabyte-scale datasets. 

* Apache Beam is an important development to be familiar with; Beam jobs are intended to be portable across "runners," which include Cloud Dataflow, and enable you to focus on your logical computation, rather than how a "runner" works -- In comparison, when authoring a Spark job, your code is bound to the runner, Spark, and how that runner works


## Large scale data wrangling ... the AWS approach in a nutshell



## Large scale data wrangling ... the Azure approach in a nutshell

