---
title: Toolchain ... for now
subtitle: The asynch.WORK will change
date: 2021-02-01
tags: ["tool", "discipline"]
---


The half-life of a knowledge worker's skill set is maybe something like five years, maybe it's longer or shorter but the only constant is change.  MUCH better tools come along ... there will never be a substitute for serious strategy of [partially-automated, continuously re-calibrated, optimized and mastered] professional listening.

Of course, we must continually develop some aptitude with new tools, new skills, new frameworks, new languages ... but the only guarantee is that the asynch.WORK toolchain will change and IMPROVE.  Thus, implicitly the first "tool" involves LISTENING, developing a practical set of [intelligence gathering disciplines](https://en.wikipedia.org/wiki/List_of_intelligence_gathering_disciplines) to INDEPENDENTLY inform one's understanding of why certain tools, frameworks, algorithms, language constructs just become part of the jargon, why other forms of information are either poisonous narrative-driven programming or just temporary shiny flashes in the pan ... professional listening means curating [knowledge graphs](https://en.wikipedia.org/wiki/Knowledge_graph) and developing new networks and relationships for better [humint](https://en.wikipedia.org/wiki/Human_intelligence_(intelligence_gathering)), [sigint](https://en.wikipedia.org/wiki/Signals_intelligence), [imint](https://en.wikipedia.org/wiki/Imagery_intelligence) or [masint](https://en.wikipedia.org/wiki/Measurement_and_signature_intelligence).


