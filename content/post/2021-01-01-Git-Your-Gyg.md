---
title: Git Your Gyg
subtitle: Relentlessly recursive dogfooding of EVERTHING
tags: ["git", "discipline"]
---

If you don't get the NEED for relentlessness of GYG.be ... you don't understand it ... frankly, we didn't understand this need until recently. Maybe 2020 furnished enough changes to spark our thinking ... maybe we just didn't get it before.

The impetus for GYG.be ... or Git your Gyg sorta started a decade ago with thinking about [data-driven programming language trends](https://githut.info) which leads to a [real-world data-driven](https://www.gharchive.org/) situational or professional career awareness, ie it's not just about what language devs actually USING but it's more about the awarenss of how development is being done NOW, how data and code drive the langauge trends for applications being developed and what that means for the evolution of culture, society, economics and the larger tech space ...  professional career awareness is about WILL be needed skillwise in the future, rather than being a victim of circumstance OR a fossilized caricature of one's PAST resume ...  of course, there's a META aspect also, in terms of learning how to use [public, widely-adopted, readily-available data wrangling tools](https://cloud.google.com/bigquery/docs) in order to LEARN how to use the data to LEARN ... but that's all part of the larger SITUATION one operates in.

Well, that WAS the case a decade ago ... and maybe still is, to some extent.  

The big idea is about an aggressively forward-leaning approach to the *forward-thinking-ness* of it ... how does one move one's thinking to where it MATTERS; not where one is now, but where one WILL BE and NEEDS TO BE ... especially, since 99.999% of the world can't stop the idiotic nonsense of living in the past or still being a captive to their inner Al Bundy, ie how they scored four touchdowns in the big high school game that happened decades ago.

Git Your Gyg is about LEARNING and intelligence gathering ... the ultimate OBJECTIVE of this intelligence gathering mission is about ***aggressively learning how to make oneself useful*** [to at least oneself, but also friends who share the same objective.]

# Intelligence gathering is too important to be left for SOMEBODY else.

You must LEARN how to gather your intelligence and you must LEARN how to constantly level up your skills when it comes to your own [professional career] intelligence gathering ... it's all about LEARNING what you should be LEARNING about in order LEARN about the next big thing that YOU will do.

It CANNOT depend on just accepting someone else's intell ... you DOGFOOD in order to know what you can trust and what you can't AND to know why your own process for intell gathering works as well as knowing something about where the blindspots are.  

**If you don't dogfood your intell, you are SWALLOWING someone else's poison.**

Recursively [DogfoodTHINKING](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) EVERYTHING is what GYG is about ... GYG.be is about ***BEING*** GYG ... it's about constantly dev'ing, testing, debugging, implementing, monitoring, adjusting and BEING your own fitness and career intell-gathering living/thinking distributed version-controlled machine. Of course, it's about dev'ing, using, mastering things like AI/ML or health/fitness ... but it is mostly about YOU and your DISCIPLINE. 

[DogfoodTHINKING](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) EVERYTHING ... not college ... is how you LEARN whether stuff actually works or does not work ... colleges are ONLY antiquated institutions for credentialling you with worthless academicTHINK and for networking with the kind of privileged assholes who go to college INSTEAD of understanding how things actually work.  When you [DogfoodDevOp](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) EVERYTHING your own personal infrastructure, you understand why things work or why other things fail, eg why we get sick, why our immune response breaks down.  There's a gigantic amount of knowledge you need, but if you actually want to LEARN it, you can't just accept the advice of experts on anything, you [DogfoodDevOp](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) EVERYTHING (eg, AI/ML, data APIs, code, health, nutrition, fitness, martial arts and rehab, time mgmt, all relationship-building and networking, jobhunting, investing, venture philanthropy, EVERY DAMNED THING you care about) in your own personal infrastructure as much as you possibly can to make it work ... and it won't work; it will be frustrating getting to root cause rather than resorting to the easy button as almost all affluent people do ... but [DogfoodDevOp'ing](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) EVERYTHING your own personal infrastructure ... there's no penalty for plagiarism or stealing good ideas, but there IS a penalty for ALMOST getting it to work well enough to serve your needs -- the TEST is whether it serves your needs. It's up to YOU, you ALONE, to dogfood-dev your shit to work well enough to serve YOUR needs.

It's not just Git ... it's also GitHub, Gitlab, GitKraken, awareness of AWESOME Git repos, awareness of everything Git -- it's computer-assisted [but not exactly computerTHINK-driven] distributed version-controlled repos of git-fied EVERYTHING ... well, almost ... public open source repos shared with the world and PRIVATE individual "proprietary" but still cloneable, version-controlled data ... but ***NEVER ultra-sensitive personal data and completely stupid things to have in an online storage anywhere like passwords or authenitication codes.***  In other words, gitTHINK everything.

